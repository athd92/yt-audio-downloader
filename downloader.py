from __future__ import unicode_literals
import youtube_dl
from youtube_dl import DownloadError
import time


def download_media(url, opt='bestaudio/best', folder_path=''):
    ydl_opts = {
        'format': opt,
        'outtmpl': folder_path + '/%(title)s.%(ext)s',
    }

    try:
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download([url])
            return True
    except:
        return False
