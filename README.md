# yt-audio-downloader

A simple YT audio downloader.

Installation steps:

```sh
git clone https://gitlab.com/athd92/yt-audio-downloader.git
cd yt-audio-downloader
pip install -r requirements.txt
python3 main.py
```

**Cautions**

Know that using a playlist url will download **ALL** of it.
Make sure a path is selected.

This application is a POC, currently in dev mode. 
Errors and bugs are NOT corrected yet.

Enjoy
