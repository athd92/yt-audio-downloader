import time
import tkinter as tk
from tkinter import filedialog, scrolledtext
from tkinter.ttk import *
import os
from downloader import download_media

BG_COLOR = '#DCDCDC'


def log_entry(txt):
    list_medias.insert(tk.INSERT,
f"""
-> {txt}""")
    window.update_idletasks()


def exit_app():
    print('Exit..')
    exit()


def get_content(filename):
    list_content.delete('1.0', tk.END)
    list_content.insert(tk.INSERT, 'MEDIAS IN SELECTED PATH')
    for f in os.listdir(filename):
        time.sleep(0.1)
        list_content.insert(tk.INSERT,
f"""
-> {f}""")
        window.update_idletasks()


def browse_button():
    global folder_path
    filename = filedialog.askdirectory()
    folder_path.set(filename)
    current_path.set(filename)
    window.update_idletasks()
    get_content(filename)
    log_entry(f'Path selected: {filename}')


def anim_progressbar():
    GB = 100
    download = 0
    speed = 1
    while download < GB:
        time.sleep(0.03)
        download_bar['value'] += (speed / GB) * 100
        download += speed
        content_bar.set(str(int((download / GB) * 100)) + '%')
        window.update_idletasks()
    download_bar['value'] = 0
    content_bar.set('Done!')


def get_media():
    url = url_input.get()
    if len(url) == 0:
        log_entry('No target added, please add a target :)')
    else:
        log_entry('Command received')
        log_entry(f'Target url: {url}')
        time.sleep(0.4)
        log_entry('Start downloading...')
        opt = 'bestaudio/best'  # feature comming up soon :)
        response = download_media(url, opt, folder_path.get())
        if response:
            anim_progressbar()
            log_entry('Done')
            url_input.delete(0, 55)
            window.update_idletasks()
        else:
            log_entry('Error append')
            log_entry(response)
            log_entry('Please try again')


def clear_input():
    url_input.delete(0, 55)
    content_bar.set('')


def clear_logs():
    list_medias.delete('1.0', tk.END)
    window.update_idletasks()


def clear_inventory():
    list_content.delete('1.0', tk.END)
    window.update_idletasks()


window = tk.Tk()
window.title('Youtube Downloader')
# window.geometry('1500x550')
window.state('zoomed')
# window.config(background=BG_COLOR)
window.resizable(width=0, height=0)

#  FRAMES

frame = tk.Frame(
    window,
    highlightbackground="black",
    highlightthickness=1,
    bd=0,
)

frame_datas = tk.Frame(
    window,
    highlightbackground="black",
    highlightthickness=1,
    bd=0,
    width=20,
    height=20,
)

frame_content = tk.Frame(
    window,
    highlightbackground="black",
    highlightthickness=1,
    bd=0,
    width=20,
    height=20,
)
#  END FRAMES

#  WIDGETS

audio_choice = tk.StringVar(value=1)
radio_audio = tk.Checkbutton(
    frame,
    text="Audio",
    variable=audio_choice,
    state="disabled",
)

label_title = tk.Label(
    frame,
    text='Enter desired url to get your media:',
    font=('Arial', 12),
    fg='black',
)

label_progress_bar = tk.Label(
    frame,
    text='Download progress:',
    font=('Arial', 12),
    fg='grey',
)

space = tk.Label(frame, text="")

current_path = tk.StringVar()
path_label = tk.Label(
    frame,
    text='Current path: /',
    textvariable=current_path,
)

button_exit = tk.Button(
    frame,
    text='Exit',
    font=('Arial', 12),
    bg='#b00000',
    fg='white',
    command=exit_app,
)

button_get_media = tk.Button(
    frame,
    text='Download',
    font=('Arial', 12),
    bg='#4AB62F',
    fg='white',
    command=get_media,
)

button_clear = tk.Button(
    frame,
    text='Clear',
    font=('Arial', 12),
    bg='#5F9EA0',
    fg='white',
    command=clear_input,
)

download_bar = Progressbar(
    frame,
    orient=tk.HORIZONTAL,
    length=200,
)

list_label = tk.Label(
    frame_datas,
    text='Logs',
    font=('Arial', 12),
    fg='grey',
)

content_label = tk.Label(
    frame_content,
    text='Selected folder content',
    font=('Arial', 12),
    fg='grey',
)

button_clear_logs = tk.Button(
    frame_datas,
    text='Clear logs',
    font=('Arial', 12),
    bg='#5F9EA0',
    fg='white',
    command=clear_logs,
)

button_clear_inventory = tk.Button(
    frame_content,
    text='Clear inventory',
    font=('Arial', 12),
    bg='#5F9EA0',
    fg='white',
    command=clear_inventory,
)

list_medias = scrolledtext.ScrolledText(
    frame_datas,
    bg="black",
    fg='#32CD32',
    width=50,
)

list_content = scrolledtext.ScrolledText(
    frame_content,
    bg="white",
    fg='grey',
    width=50,
)

content_bar = tk.StringVar()
percent_label = tk.Label(
    frame,
    textvariable=content_bar,
)

entry_value = tk.StringVar()
url_input = tk.Entry(frame, width=55, textvariable=entry_value)

folder_path = tk.StringVar()

label_browse = tk.Label(frame, textvariable=folder_path)
button_browse = tk.Button(frame, text="Browse", command=browse_button)

# END WIDGETS


#  MAIN FAME

label_title.grid(row=0, column=0)
space.grid(row=1, column=0)

url_input.grid(row=2, column=0, padx=15, pady=5)
label_progress_bar.grid(row=3, column=0)

download_bar.grid(row=4, column=0, padx=15, pady=5)
percent_label.grid(row=5, column=0, padx=15, pady=5)

radio_audio.grid(row=6, column=0)

button_browse.grid(row=7, column=0)
path_label.grid(row=8, column=0)
button_get_media.grid(row=9, column=0, pady=5, padx=5)

button_clear.grid(row=10, column=0,pady=5, padx=5)
button_exit.grid(row=11, column=0, pady=5, padx=5)

frame.grid(row=0, column=0, padx=50, pady=10)

# END MAIN FRAME

# FRAME DATAS

list_label.grid(row=0, column=0)
list_medias.grid(row=1, column=0, padx=5, pady=5, sticky='nsew')
button_clear_logs.grid(row=2, column=0, pady=5, padx=5)
frame_datas.grid(row=0, column=1, padx=50, pady=10)

# END FRAME DATAS

# FRAME CONTENT

content_label.grid(row=0, column=0)
list_content.grid(row=1, column=0, padx=5, pady=5, sticky='nsew')
button_clear_inventory.grid(row=2, column=0, pady=5, padx=5)
frame_content.grid(row=0, column=2)

# END FRAME CONTENT


window.mainloop()
